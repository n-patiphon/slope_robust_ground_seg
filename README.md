#slope_robust_ground_seg
An implementation of a ground segmentation algorithm proposed in "A Slope-robust Cascaded Ground Segmentation in 3D Point Cloud for Autonomous Vehicles" and the labeled dataset

## Requirements
* Python 2.7
* [python-pcl](https://github.com/strawlab/python-pcl)
* [pypcd](https://github.com/dimatura/pypcd)

## Parameters
Parameters of the algorithm can be set using conf file

## Run
```bash
$ python slope_robust_cascaded_ground_segmentation.py input_file.pcd output_file_name_without_extension
```

## Example
```bash
$ python slope_robust_cascaded_ground_segmentation.py raw_scans/full_cloud0.pcd scene0
```

The output clouds will be saved in output 
