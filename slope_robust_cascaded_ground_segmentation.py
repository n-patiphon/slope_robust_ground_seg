import pcl
import pypcd
import pandas as pd
import numpy as np
import math
import sys
import ConfigParser
from timeit import default_timer as timer

def to_DataFrame(pc):
    return pd.DataFrame(pc.pc_data, columns=pc.fields)

def radius_cal(theta, alpha, beta):
    return (1.0/(math.tan(theta) + math.tan(beta))) - (1.0/(math.tan(alpha + theta) + math.tan(beta)))

def radius_table(model, height, slope):
    if model == 16:
        step = 2.0
        initial_angle = -15.0
    elif model == 32:
        step = 4.0/3.0
        initial_angle = -31.0/3.0
    elif model == 64:
        step = 1.0/3.0
        initial_angle = -2.0
    else:
        step = 1.0/3.0
        initial_angle = -2.0

    r_table = pd.DataFrame(index=range(2), columns=range(model))
    alpha = math.radians(step)
    beta = math.radians(slope)

    for ray in r_table.columns:
        theta = math.radians(ray*step + initial_angle)
        if ((model == 64) & (ray == 31)):
            r_table[ray][0] = height*radius_cal(theta, -1*alpha, beta)
            if theta != 0: r_table[ray][1] = height/math.tan(theta)
            step = 0.5
            initial_angle = -15.0 + 8.83
            alpha = math.radians(step)
            beta = math.radians(slope)
        else:
            r_table[ray][0] = height*radius_cal(theta, alpha, beta)
            if theta != 0: r_table[ray][1] = height/math.tan(theta)
    return r_table

def depth_image(cloud_df, model, width=0):
    if width == 0:
        if model == 16:
            width = 1800
        elif model == 32:
            width = 2250
        else:
            width = 2083

    d_image = np.empty((model, width))
    d_image.fill(np.nan)
    cloud_df = full_cloud_df
    arr = cloud_df.values
    size = arr.shape[0]
    for index in range(size):
        u = math.degrees(math.atan2(arr[index][1], arr[index][0]))
        if u < 0:
            u += 360
        col = width - int(width*u/360.0) - 1
        ind = model - arr[index][4] - 1
        d_image[ind][col] = index
    d_image = pd.DataFrame(d_image, index=range(model), columns=range(width))
    return d_image

def estimated_rad(tar_ind, ref_ind, r_table):
    r = 0
    for i in range(ref_ind, tar_ind):
        r += r_table[i][0]
    return r

def inter_ring_filter(scan_col, r_table):
    """
    This function performs ground points filtering in the first step of the proposed algorithm
    """

    r_ref = 0
    ref_ind = 0

    vertical_points = []
    ground_points = []
    candidates = []

    indices = []
    arr = []
    for ring, ind in scan_col.iteritems():
        indices.append(ring)
        arr.append(ind)

    indices = np.asarray(indices)
    arr = np.asarray(arr)
    arr_len = arr.size

    point0 = full_cloud_df.loc[arr[0]]

    candidates.append(point0)
    z_max = point0.z
    z_min = point0.z

    for i in range(arr_len):
        if i != (arr_len - 1):
            ref_ind = indices[i]
            ref_cloud = arr[i]
            point_ref = full_cloud_df.loc[ref_cloud]
            r_ref = np.sqrt(point_ref.x**2 + point_ref.y**2)

            tar_ind =  indices[i + 1]
            tar_cloud = arr[i + 1]
            point_tar = full_cloud_df.loc[tar_cloud]
            r_tar = np.sqrt(point_tar.x**2 + point_tar.y**2)

            r_diff = abs(r_ref - r_tar)
            if ((r_diff < estimated_rad(tar_ind, ref_ind, r_table))):
                candidates.append(point_tar)
                if (point_tar.z > z_max): z_max = point_tar.z
                if (point_tar.z < z_min): z_min = point_tar.z
            else:
                if (len(candidates) > 1) and ((z_max - z_min) > vertical_thres):
                    vertical_points += candidates
                    candidates = []
                else:
                    ground_points += candidates
                    candidates = []
                candidates.append(point_tar)
                z_max = point_tar.z
                z_min = point_tar.z
        else:
            if (len(candidates) > 1) and ((z_max - z_min) > vertical_thres):
                vertical_points += candidates
                candidates = []
            else:
                ground_points += candidates
                candidates = []

    return vertical_points, ground_points

def cal_u(a):
    u = math.degrees(a)
    if u < 0:
        u += 360
    return u
# Devide a scan into quadrants
def add_quadrants(in_df, model):

    width = 0
    if model == 16:
        width = 1800
    elif model == 32:
        width = 2250
    else:
        width = 2083

    in_df['col'] = np.arctan2(in_df['y'], in_df['x']).apply(cal_u)
    in_df['col'] = width - 1 - (in_df['col']*width/360.0).apply(int)
    in_df['quadrant'] = in_df['col'] - in_df['col']

    h_section_w = int(math.ceil(width/8.0))
    bounds = [(width - h_section_w, h_section_w), (h_section_w, 3*h_section_w), (3*h_section_w, 5*h_section_w), (5*h_section_w, width - h_section_w)]

    sec = 0
    for bound in bounds:
        if sec == 0:
            in_df.loc[(in_df['col'] < bound[1]), 'quadrant'] = sec
        elif sec == len(bounds) - 1:
            in_df.loc[(in_df['col'] >= bound[0]), 'quadrant'] = sec
        else:
            in_df.loc[(in_df['col'] >= bound[0]) & (in_df['col'] < bound[1]), 'quadrant'] = sec
        sec += 1

    in_df.drop('col', axis=1, inplace=True)
# Devide a quadrant into segments
def add_sections(in_df, section, model, r_table):
    in_df['radius'] = np.sqrt(in_df['x']*in_df['x'] + in_df['y']*in_df['y'])
    in_df['section'] = in_df['radius'] - in_df['radius']
    width = int(math.ceil(model/section))
    bounds = []
    for i in range(section):
        if i == (section-1):
            bounds.append((r_table.iloc[1].iloc[-width*(i) - 1], r_table.iloc[1].iloc[0]))
        else:
            bounds.append((r_table.iloc[1].iloc[-width*(i) - 1], r_table.iloc[1].iloc[-width*(i + 1) - 1]))
    sec = 0
    for bound in bounds:
        if sec == 0:
            in_df.loc[(in_df['radius'] < bound[1]), 'section'] = sec
        elif sec == len(bounds) - 1:
            in_df.loc[(in_df['radius'] >= bound[0]), 'section'] = sec
        else:
            in_df.loc[(in_df['radius'] >= bound[0]) & (in_df['radius'] < bound[1]), 'section'] = sec
        sec += 1
    in_df.drop('radius', axis=1, inplace=True)

def as_unit(a):
    u = np.asarray(a)
    norm = np.sqrt(a[0]**2 + a[1]**2 + a[2]**2)
    return u/norm

def get_xy(q, s, r_table):
    unit = [(1, 0), (0, -1), (-1, 0), (0, 1)]
    width = int(math.ceil(model/sections))
    r = r_table.iloc[1].iloc[-width*s - 1]
    return unit[q][0]*r, unit[q][1]*r

def validate(c1, c2, q, s, r_table):
    unit1 = as_unit(c1)[:2]
    unit2 = as_unit(c2)[:2]
    ang_diff = math.degrees(math.acos(np.dot(unit1, unit2)))
    x, y = get_xy(q, s, r_table)
    h_diff = abs((c2[0]/c2[2] - c1[0]/c1[2])*x + (c2[1]/c2[2] - c1[1]/c1[2])*y + (c2[3]/c2[2] - c1[3]/c1[2]))
    return ang_diff, h_diff
# Plane segmentation
def simple_plane_seg(points, coeff, distance_thres=0.3):
    u_coeff = as_unit(coeff)
    x = points.values
    indices = np.where(abs(u_coeff[0]*x[:,0] + u_coeff[1]*x[:,1] + u_coeff[2]*x[:,2] + u_coeff[3]) < distance_thres)[0]
    return indices, coeff

def validated_plane_seg(in_df, quad, section, r_table, pre_coeff=None, distance_thres=0.3, nor_weight=0.5):

    points = in_df[['x', 'y', 'z']].astype('float32')

    cloud = pcl.PointCloud()
    cloud.from_array(points.values)

    # RANSAC plane segmentation
    seg = cloud.make_segmenter_normals(ksearch=50)
    seg.set_optimize_coefficients(True)
    seg.set_model_type(pcl.SACMODEL_NORMAL_PLANE)
    seg.set_method_type(pcl.SAC_RANSAC)
    seg.set_distance_threshold(distance_thres)
    seg.set_normal_distance_weight(nor_weight)
    seg.set_max_iterations(100)
    indices, coefficients = seg.segment()

    if pre_coeff is not None:
        diff_ang, diff_height = validate(pre_coeff, coefficients, quad, section, r_table)
        if (diff_ang < plane_ang_thres) & (diff_height < plane_height_thres):
            print('Discontinuity detected')
            indices, coefficients = simple_plane_seg(points, pre_coeff, distance_thres)

    total = in_df.shape[0]
    vert_ind = range(total)
    vert_ind = list(set(vert_ind) - set(indices))

    vertical_df = points.iloc[vert_ind]
    ground_df = points.iloc[indices]

    return vertical_df, ground_df, coefficients

def validated_multi_plane_segment_ground(in_df, r_table=None):
    if r_table is None:
        r_table = radius_table(model, height, slope)

    add_quadrants(in_df, model)
    add_sections(in_df, section=sections, model=model, r_table=r_table)

    vertical_df = pd.DataFrame()
    ground_df = pd.DataFrame()

    for q in range(4):
        prev_coeffs = []
        for s in range(sections):
            if s == 0:
                v_df, g_df, coeff = validated_plane_seg(in_df[(in_df['quadrant'] == q) & (in_df['section'] == s)], q, s, r_table=r_table)
            else:
                v_df, g_df, coeff = validated_plane_seg(in_df[(in_df['quadrant'] == q) & (in_df['section'] == s)], q, s, r_table=r_table, pre_coeff=prev_coeffs)
            vertical_df = pd.concat([vertical_df, v_df], axis=0)
            ground_df = pd.concat([ground_df, g_df], axis=0)
            prev_coeffs = coeff

    return vertical_df, ground_df

def cascased_ground_seg(in_df):
    r_table = radius_table(model, height, slope)
    d_image = depth_image(in_df, model)
    vertical_arr = []
    ground_arr = []
    for column in d_image.columns:
        vertical_points, ground_points = inter_ring_filter(d_image[column].dropna(), r_table)
        vertical_arr += vertical_points
        ground_arr += ground_points

    vertical_arr = np.asarray(vertical_arr)
    vertical_df = pd.DataFrame(vertical_arr, columns=in_df.columns)
    ground_arr= np.asarray(ground_arr)
    ground_df = pd.DataFrame(ground_arr, columns=in_df.columns)

    # Second filter
    added_ver_df, ground_df = validated_multi_plane_segment_ground(ground_df, r_table)
    vertical_df = pd.concat([vertical_df[['x', 'y', 'z']], added_ver_df], axis=0)

    return vertical_df, ground_df

if __name__=='__main__':

    # Parameters
    config = ConfigParser.ConfigParser()
    config.readfp(open(r'conf'))
    model = int(config.get('params', 'Total number of laser emitter of LIDAR'))
    height = float(config.get('params', 'LIDAR height'))
    slope = float(config.get('params', 'Beta'))
    vertical_thres = float(config.get('params', 'Cluster height threshold'))
    distance_thres = float(config.get('params', 'Plane distance threshold'))
    sections = int(config.get('params', 'Number of section'))
    plane_ang_thres = float(config.get('params', 'Angular difference threshold'))
    plane_height_thres = float(config.get('params', 'Height difference threshold'))

    print('Parameters used for segmentation are as follows:')
    print('Total number of laser emitter of LIDAR: {}'.format(model))
    print('LIDAR height: {}'.format(height))
    print('Beta: {}'.format(slope))
    print('Cluster height threshold: {}'.format(vertical_thres))
    print('Plane distance threshold: {}'.format(distance_thres))
    print('Number of section: {}'.format(sections))
    print('Angular difference threshold: {}'.format(plane_ang_thres))
    print('Height difference threshold: {}'.format(plane_height_thres))

    file_name = sys.argv[1]
    full_cloud = pypcd.PointCloud.from_path(file_name)
    full_cloud_df = to_DataFrame(full_cloud)

    print('Performing ground segmentation in '+file_name + '...')
    start = timer()
    vertical_df, ground_df = cascased_ground_seg(full_cloud_df)
    elap = timer() - start
    print('Computational time: {a}'.format(a=elap))

    ground = np.asarray(ground_df)
    vertical = np.asarray(vertical_df)
    g_cloud = pcl.PointCloud()
    g_cloud.from_array(ground)
    v_cloud = pcl.PointCloud()
    v_cloud.from_array(vertical)
    output = sys.argv[2]
    pcl.save(g_cloud, './' + 'output' + '/' + output + '_ground.pcd', binary=False)
    pcl.save(v_cloud, './' + 'output' + '/' + output + '_vertical.pcd', binary=False)
    print('Output files saved')
    print('Done!')
